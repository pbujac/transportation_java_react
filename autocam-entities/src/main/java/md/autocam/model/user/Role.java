package md.autocam.model.user;

import md.autocam.model.BaseModel;

import javax.persistence.Entity;

/**
 * Created by pit on 4/28/17.
 */
@Entity
public class Role extends BaseModel {

    private String roleName;

    public Role() {
    }

    public Role(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
