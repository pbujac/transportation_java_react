package md.autocam.model.employee;

import md.autocam.model.BaseModel;
import md.autocam.model.person.LegalPerson;
import md.autocam.model.person.NaturalPerson;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class Employee extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "company_id", referencedColumnName="id")
    private LegalPerson company;

    @ManyToOne
    @JoinColumn(name = "natural_person_id", referencedColumnName="id")
    private NaturalPerson person;

    @ManyToOne
    @JoinColumn(name = "employee_job_id", referencedColumnName="id")
    private EmployeeJob employeeJob;

    public Employee() {
    }

    public Employee(LegalPerson company, NaturalPerson person, EmployeeJob employeeJob) {
        this.company = company;
        this.person = person;
        this.employeeJob = employeeJob;
    }

    public NaturalPerson getPerson() {
        return person;
    }

    public void setPerson(NaturalPerson person) {
        this.person = person;
    }

    public EmployeeJob getEmployeeJob() {
        return employeeJob;
    }

    public void setEmployeeJob(EmployeeJob employeeJob) {
        this.employeeJob = employeeJob;
    }

    public LegalPerson getCompany() {
        return company;
    }

    public void setCompany(LegalPerson company) {
        this.company = company;
    }
}
