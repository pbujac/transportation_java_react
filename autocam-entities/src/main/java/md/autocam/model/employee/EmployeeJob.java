package md.autocam.model.employee;

import md.autocam.model.BaseModel;

import javax.persistence.Entity;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class EmployeeJob extends BaseModel {

    private String employeeType;

    public EmployeeJob() {
    }

    public EmployeeJob(String employeeType) {
        this.employeeType = employeeType;
    }

    public String getEmployeeType() {
        return employeeType;
    }

    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }
}
