package md.autocam.model.medical_evidence;

import md.autocam.model.BaseModel;
import md.autocam.model.roadmap.Roadmap;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class MedicalEvidence extends BaseModel {

    private Float pulse;
    private Float bodyTemperature;
    private Float accusation;
    private String bloodPressure;
    private String alchoolDrugsConclusion;
    private String conclusion;

    @ManyToOne
    @JoinColumn(name = "roadmap_id", referencedColumnName="id")
    private Roadmap roadmap;

    public MedicalEvidence() {
    }

    public Float getPulse() {
        return pulse;
    }

    public void setPulse(Float pulse) {
        this.pulse = pulse;
    }

    public Float getBodyTemperature() {
        return bodyTemperature;
    }

    public void setBodyTemperature(Float bodyTemperature) {
        this.bodyTemperature = bodyTemperature;
    }

    public Float getAccusation() {
        return accusation;
    }

    public void setAccusation(Float accusation) {
        this.accusation = accusation;
    }

    public String getBloodPressure() {
        return bloodPressure;
    }

    public void setBloodPressure(String bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public String getAlchoolDrugsConclusion() {
        return alchoolDrugsConclusion;
    }

    public void setAlchoolDrugsConclusion(String alchoolDrugsConclusion) {
        this.alchoolDrugsConclusion = alchoolDrugsConclusion;
    }

    public String getConclusion() {
        return conclusion;
    }

    public void setConclusion(String conclusion) {
        this.conclusion = conclusion;
    }

    public Roadmap getRoadmap() {
        return roadmap;
    }

    public void setRoadmap(Roadmap roadmap) {
        this.roadmap = roadmap;
    }
}
