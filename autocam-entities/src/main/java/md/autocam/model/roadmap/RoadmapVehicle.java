package md.autocam.model.roadmap;

import md.autocam.model.BaseModel;
import md.autocam.model.employee.Employee;
import md.autocam.model.vehicle.Vehicle;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by pit on 5/5/17.
 */
@Entity
public class RoadmapVehicle extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "driver_id", referencedColumnName="id")
    private Employee driver;

    @ManyToOne
    @JoinColumn(name = "vehicle_id", referencedColumnName="id")
    private Vehicle vehicle;

    @ManyToOne
    @JoinColumn(name = "roadmap_id", referencedColumnName="id")
    private Roadmap roadmap;

    public RoadmapVehicle() {
    }

    public RoadmapVehicle(Employee driver, Vehicle vehicle, Roadmap roadmap) {
        this.driver = driver;
        this.vehicle = vehicle;
        this.roadmap = roadmap;
    }

    public Employee getDriver() {
        return driver;
    }

    public void setDriver(Employee driver) {
        this.driver = driver;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public Roadmap getRoadmap() {
        return roadmap;
    }

    public void setRoadmap(Roadmap roadmap) {
        this.roadmap = roadmap;
    }
}
