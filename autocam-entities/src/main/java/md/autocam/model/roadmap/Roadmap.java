package md.autocam.model.roadmap;

import md.autocam.model.BaseModel;
import md.autocam.model.address.BaseAddress;
import md.autocam.model.address.Locality;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Date;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class Roadmap extends BaseModel {

    private String series;
    private String number;
    private Date departureDate;
    private Date arrivedDate;
    private Date valabilityDate;

    @ManyToOne
    @JoinColumn(name = "departure_locality_id", referencedColumnName="id")
    private BaseAddress departureLocality;

    @ManyToOne
    @JoinColumn(name = "arrived_locality_id", referencedColumnName="id")
    private BaseAddress arrivedLocality;

    private Float combustibleQuantity;

    private Float departureDistance;
    private Float arrivedDistance;

    public Roadmap() {
    }

    public Roadmap(String series, String number, Date departureDate, Date arrivedDate,
                   BaseAddress departureLocality, BaseAddress arrivedLocality,
                   Float combustibleQuantity, Float departureDistance, Float arrivedDistance) {
        this.series = series;
        this.number = number;
        this.departureDate = departureDate;
        this.arrivedDate = arrivedDate;
        this.departureLocality = departureLocality;
        this.arrivedLocality = arrivedLocality;
        this.combustibleQuantity = combustibleQuantity;
        this.departureDistance = departureDistance;
        this.arrivedDistance = arrivedDistance;
    }

    public Date getValabilityDate() {
        return valabilityDate;
    }

    public void setValabilityDate(Date valabilityDate) {
        this.valabilityDate = valabilityDate;
    }

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public Date getArrivedDate() {
        return arrivedDate;
    }

    public void setArrivedDate(Date arrivedDate) {
        this.arrivedDate = arrivedDate;
    }

    public BaseAddress getDepartureLocality() {
        return departureLocality;
    }

    public void setDepartureLocality(BaseAddress departureLocality) {
        this.departureLocality = departureLocality;
    }

    public BaseAddress getArrivedLocality() {
        return arrivedLocality;
    }

    public void setArrivedLocality(BaseAddress arrivedLocality) {
        this.arrivedLocality = arrivedLocality;
    }

    public Float getCombustibleQuantity() {
        return combustibleQuantity;
    }

    public void setCombustibleQuantity(Float combustibleQuantity) {
        this.combustibleQuantity = combustibleQuantity;
    }

    public Float getDepartureDistance() {
        return departureDistance;
    }

    public void setDepartureDistance(Float departureDistance) {
        this.departureDistance = departureDistance;
    }

    public Float getArrivedDistance() {
        return arrivedDistance;
    }

    public void setArrivedDistance(Float arrivedDistance) {
        this.arrivedDistance = arrivedDistance;
    }
}
