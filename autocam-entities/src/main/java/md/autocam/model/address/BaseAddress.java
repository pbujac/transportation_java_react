package md.autocam.model.address;

import md.autocam.model.BaseModel;

import javax.persistence.Entity;
import javax.persistence.MappedSuperclass;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class BaseAddress extends BaseModel {

    private String name;

    public BaseAddress() {
    }

    public BaseAddress(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
