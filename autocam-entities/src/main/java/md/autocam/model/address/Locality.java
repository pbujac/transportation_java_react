package md.autocam.model.address;

import md.autocam.model.person.LegalPersonType;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by pit on 5/1/17.
 */
//@Entity
public class Locality extends BaseAddress {

    @ManyToOne
    @JoinColumn(name = "region_id", referencedColumnName="id")
    private Region region;

    public Locality() {
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }
}
