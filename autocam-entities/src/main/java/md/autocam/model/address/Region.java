package md.autocam.model.address;

import md.autocam.model.person.LegalPersonType;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by pit on 5/1/17.
 */
//@Entity
public class Region extends BaseAddress {

    @ManyToOne
    @JoinColumn(name = "country_id", referencedColumnName="id")
    private Country country;

    public Region() {
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}
