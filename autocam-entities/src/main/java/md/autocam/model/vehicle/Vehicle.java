package md.autocam.model.vehicle;

import md.autocam.model.BaseModel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class Vehicle extends BaseModel {

    private String plateNumber;

    @ManyToOne
    @JoinColumn(name = "model_id", referencedColumnName="id")
    private VehicleModel model;

    public Vehicle() {
    }

    public Vehicle(String plateNumber, VehicleModel model) {
        this.plateNumber = plateNumber;
        this.model = model;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public VehicleModel getModel() {
        return model;
    }

    public void setModel(VehicleModel model) {
        this.model = model;
    }
}
