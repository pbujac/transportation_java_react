package md.autocam.model.vehicle;

import md.autocam.model.BaseModel;

import javax.persistence.Entity;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class VehicleBrand extends BaseModel {

    private String name;

    public VehicleBrand() {
    }

    public VehicleBrand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
