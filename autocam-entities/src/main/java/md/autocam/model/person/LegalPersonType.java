package md.autocam.model.person;

import md.autocam.model.BaseModel;

import javax.persistence.Entity;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class LegalPersonType extends BaseModel {

    private String legalPersonType;

    public LegalPersonType() {
    }

    public LegalPersonType(String legalPersonType) {
        this.legalPersonType = legalPersonType;
    }

    public String getLegalPersonType() {
        return legalPersonType;
    }

    public void setLegalPersonType(String legalPersonType) {
        this.legalPersonType = legalPersonType;
    }
}
