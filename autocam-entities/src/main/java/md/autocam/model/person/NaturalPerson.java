package md.autocam.model.person;

import javax.persistence.Entity;
import java.sql.Date;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class NaturalPerson extends BasePerson {

    private String surname;
    private Date birthdayDate;

    public NaturalPerson() {
    }

    public NaturalPerson(String surname, Date birthdayDate) {
        this.surname = surname;
        this.birthdayDate = birthdayDate;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}
