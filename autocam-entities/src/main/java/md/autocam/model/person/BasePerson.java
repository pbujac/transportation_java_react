package md.autocam.model.person;

import md.autocam.model.BaseModel;
import md.autocam.model.address.BaseAddress;
import md.autocam.model.address.Locality;

import javax.persistence.CascadeType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

/**
 * Created by pit on 5/1/17.
 */

@MappedSuperclass
public class BasePerson extends BaseModel {

    private String name;
//    @ManyToOne
    @ManyToOne
    @JoinColumn(name = "address_id", referencedColumnName="id")
    private BaseAddress address;


    public BasePerson() {
        this.name = "testName";
        this.address = new BaseAddress("testAddress");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BaseAddress getAddress() {
        return address;
    }

    public void setAddress(BaseAddress address) {
        this.address = address;
    }
}
