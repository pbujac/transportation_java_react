package md.autocam.model.person;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

/**
 * Created by pit on 5/1/17.
 */
@Entity
public class LegalPerson extends BasePerson {

    private String fiscalCode;

    @ManyToOne
    @JoinColumn(name = "legal_person_type_id", referencedColumnName="id")
    private LegalPersonType type;

    public LegalPerson() {
    }

    public LegalPerson(String fiscalCode, LegalPersonType type) {
        this.fiscalCode = fiscalCode;
        this.type = type;
    }
}
