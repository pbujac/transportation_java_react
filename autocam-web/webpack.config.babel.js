import { resolve } from 'path';
import webpack from 'webpack';

export default {
  context: resolve(__dirname, 'src/main/webapp/public/js'),
  entry: [
    'babel-polyfill',
    './index.js'
  ],
  output: {
    path: resolve(__dirname, 'src/main/resources/static/js/'),
    publicPath: '/',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js?$/,
        use: [ 'babel-loader', ],
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader', ],
      },
      // {
      //   test: /\.scss$/,
      //   use: ['style-loader', 'css-loader?modules', 'postcss-loader', 'sass-loader']
      // },
      {
        test: /\.(woff2?|ttf|eot|svg)$/,
        use: ['url-loader?limit=10000', 'file-loader']
      },
      {
        test: /bootstrap\/dist\/js\/umd\//,
        use: 'imports?jQuery=jquery'
      }
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
      jQuery: 'jquery',
      $: 'jquery',
      jquery: 'jquery'
    })
  ]
};