export function driversFormattedForDropdown(drivers) {
    return drivers.map(driver => {
        return {
            value: driver.id,
            text: driver.person.name + ' ' + driver.person.surname
        };
    });
}
