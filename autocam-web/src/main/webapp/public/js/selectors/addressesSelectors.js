export function addressesFormattedForDropdown(addresses) {
    return addresses.map(address => {
        return {
            value: address.id,
            text: address.name
        };
    });
}
