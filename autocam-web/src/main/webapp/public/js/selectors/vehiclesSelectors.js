export function vehiclesFormattedForDropdown(vehicles) {
    return vehicles.map(vehicle => {
        return {
            value: vehicle.id,
            text: vehicle.plateNumber + ' ' + vehicle.model.brand.name + ' ' + vehicle.model.name
        };
    });
}
