import * as types from '../actions/actionTypes';
import roadmapsApi from '../api/roadmapsApi';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function loadRoadmapsSuccess(roadmaps) {
  return {type: types.LOAD_ROADMAPS_SUCCESS, roadmaps};
}

export function updateRoadmapSuccess(roadmap) {
    return { type: types.UPDATE_ROADMAP_SUCCESS, roadmap };
}
export function createRoadmapSuccess(roadmap) {
    return { type: types.CREATE_ROADMAP_SUCCESS, roadmap };
}

export function loadRoadmaps() {
  return function(dispatch) {
    return roadmapsApi.getAllRoadmaps().then(roadmaps => {
      console.log(roadmaps);
      dispatch(loadRoadmapsSuccess(roadmaps));
    })
      .catch(error => {
      throw(error);
    });
  };
}

export function saveRoadmap(roadmap) {
    return function (dispatch, getState) {
        dispatch(beginAjaxCall());
        return roadmapsApi.saveRoadmap(roadmap).then(roadmap => {
            roadmap.id ? dispatch(updateRoadmapSuccess(roadmap)) :
                dispatch(createRoadmapSuccess(roadmap));
        }).catch(error => {
            dispatch(ajaxCallError(error));
            throw(error);
        });
    };
}
