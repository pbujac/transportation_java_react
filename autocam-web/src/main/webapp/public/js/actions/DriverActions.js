import * as types from '../actions/actionTypes';
import driversApi from '../api/driversApi';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function loadDriversSuccess(drivers) {
    return {type: types.LOAD_DRIVERS_SUCCESS, drivers};
}

export function updateDriverSuccess(driver) {
    return { type: types.UPDATE_DRIVER_SUCCESS, driver };
}
export function createDriverSuccess(driver) {
    return { type: types.CREATE_DRIVER_SUCCESS, driver };
}

export function loadDrivers() {
    return function(dispatch) {
        return driversApi.getAllDrivers().then(drivers => {
            console.log(drivers);
            dispatch(loadDriversSuccess(drivers));
        })
            .catch(error => {
                throw(error);
            });
    };
}

export function saveDriver(driver) {
    return function (dispatch, getState) {
        dispatch(beginAjaxCall());
        return driversApi.saveDriver(driver).then(driver => {
            driver.id ? dispatch(updateDriverSuccess(driver)) :
                dispatch(createDriverSuccess(driver));
        }).catch(error => {
            dispatch(ajaxCallError(error));
            throw(error);
        });
    };
}
