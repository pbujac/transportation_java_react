import * as types from '../actions/actionTypes';
import vehiclesApi from '../api/vehiclesApi';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function loadVehiclesSuccess(vehicles) {
  return {type: types.LOAD_VEHICLES_SUCCESS, vehicles};
}

export function updateVehicleSuccess(vehicle) {
    return { type: types.UPDATE_VEHICLE_SUCCESS, vehicle };
}
export function createVehicleSuccess(vehicle) {
    return { type: types.CREATE_VEHICLE_SUCCESS, vehicle };
}

export function loadVehicles() {
  return function(dispatch) {
    return vehiclesApi.getAllVehicles().then(vehicles => {
      console.log(vehicles);
      dispatch(loadVehiclesSuccess(vehicles));
    })
      .catch(error => {
      throw(error);
    });
  };
}

export function saveVehicle(vehicle) {
    return function (dispatch, getState) {
        dispatch(beginAjaxCall());
        return vehiclesApi.saveVehicle(vehicle).then(vehicle => {
            vehicle.id ? dispatch(updateVehicleSuccess(vehicle)) :
                dispatch(createVehicleSuccess(vehicle));
        }).catch(error => {
            dispatch(ajaxCallError(error));
            throw(error);
        });
    };
}
