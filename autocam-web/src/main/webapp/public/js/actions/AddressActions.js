import * as types from '../actions/actionTypes';
import addressesApi from '../api/addressApi';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function loadAddressesSuccess(addresses) {
    return {type: types.LOAD_ADDRESSES_SUCCESS, addresses};
}

export function updateDriverSuccess(address) {
    return { type: types.UPDATE_ADDRESS_SUCCESS, address };
}
export function createDriverSuccess(address) {
    return { type: types.CREATE_ADDRESS_SUCCESS, address };
}

export function loadAddresses() {
    return function(dispatch) {
        return addressesApi.getAllAddresses().then(addresses => {
            console.log(addresses);
            dispatch(loadAddressesSuccess(addresses));
        })
            .catch(error => {
                throw(error);
            });
    };
}

export function saveAddress(address) {
    return function (dispatch, getState) {
        dispatch(beginAjaxCall());
        return addressesApi.saveAddress(address).then(address => {
            address.id ? dispatch(updateDriverSuccess(address)) :
                dispatch(createDriverSuccess(address));
        }).catch(error => {
            dispatch(ajaxCallError(error));
            throw(error);
        });
    };
}
