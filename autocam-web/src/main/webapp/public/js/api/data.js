export default [
    {
        "roadmap": {
            "number": "nr232",
            "series": "nde3455",
            "arrivedDate": "2017-05-21",
            "departureDate": "2017-03-12",
            "combustibleQuantity": "12.0",
            "departureDistance": "9.0",
            "arrivedDistance": "199.0",
            "valabilityDate": "2017-07-12",
            "arrivedLocality": {
                "name": "Ukraina, or.Brest, str. Lvov, 5"
            },
            "departureLocality": {
                "name": "Moldova, r. Anenii Noi, s. Mereni, str. Independentei, 5"
            }
        },
        "driver": {
            "person": {
                "name": "Vlad",
                "surname": "Volcov",
                "birthDate": "1980-02-22",
                "address": {
                    "name": "Moldova, or. Chisinau, str.Stefan cel mare, 34"
                }
            },
            "company": {
                "name": "Autocam Service",
                "fiscalCode": "1234567890123",
                "address": {
                    "name":  "Moldova, or. Chisinau, str.Stefan cel mare, 34"
                },
                "type": "SRL"
            },
            "employeeJob": {
                "employeeType": "SOFER"
            }
        },
        "vehicle": {
            "plateNumber": "CU AN 456",
            "model": {
                "name": "Camry",
                "brand": {
                    "name": "Toyota"
                }
            }

        }
    }
]