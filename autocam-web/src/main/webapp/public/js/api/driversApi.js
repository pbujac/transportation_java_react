class DriversApi {
  static getAllDrivers() {

    var base64encodedData = new Buffer('admin' + ':' + 'admin').toString('base64');
    let headers = {
      method: 'GET',
      headers: {
        'Authorization': 'Basic ' + base64encodedData
      }
    };
    let request = (url) => new Request(url, headers);

    return fetch(request(`/api/employee/search/job/SOFER`))
      .then(response => {
        return response.json();
      })
      .catch(error => {
        return error;
      });
  }
}

export default DriversApi;