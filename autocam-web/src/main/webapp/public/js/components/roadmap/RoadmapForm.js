import $  from 'jquery';
import serialize from 'form-serialize';
import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TextInput from '../common/TextInput';
import DateInput from '../common/DateInput';
import SelectInput from '../common/SelectInput';
import AutosuggestTextInput from '../common/AutosuggestTextInput';

class RoadmapForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            departureDate: '',
            arrivedDate: '',
            valabilityDate: '',
            filterText: ''
        };
        this.departureDateChangeHandler = this.departureDateChangeHandler.bind(this);
        this.arrivedDateChangeHandler = this.arrivedDateChangeHandler.bind(this);
        this.valabilityDateChangeHandler = this.valabilityDateChangeHandler.bind(this);
        this.handleFilterTextInput = this.handleFilterTextInput.bind(this);
    }
    handleFilterTextInput(filterText) {
        this.setState({
            filterText: filterText
        });
    }

    departureDateChangeHandler(date) {
        console.log(date);
        this.setState({
            departureDate: date
        });
    }
    arrivedDateChangeHandler(date) {
        console.log(date);
        this.setState({
            arrivedDate: date
        });
    }
    valabilityDateChangeHandler(date) {
        console.log(date);
        this.setState({
            valabilityDate: date
        });
    }

    render() {
        const { roadmap, allDrivers, allAddresses, allVehicles, onSave,  saving, errors } = this.props;
        return (
            <form id="roadmapForm">
                <TextInput
                    name="roadmap[series]"
                    label="Seria foaie de parcurs"
                    error={errors.series}
                />
                <TextInput
                    name="roadmap[number]"
                    label="Numar foaie de parcurs"
                    error={errors.number}
                />
                <SelectInput
                    name="drivers[0]"
                    label="Sofer"
                    defaultOption="Selectati sofer"
                    options={allDrivers}
                    error={errors.driver}
                    filterText={this.state.filterText}
                    onFilterTextInput={this.handleFilterTextInput}
                />

                <SelectInput
                    name="vehicle[id]"
                    label="Masina - numar de inmatriculare"
                    defaultOption="Selectati masina"
                    options={allVehicles}
                    error={errors.vehicle}
                    filterText={this.state.filterText}
                    onFilterTextInput={this.handleFilterTextInput}
                />

                <SelectInput
                    name="vehicle[id]"
                    label="Remorca"
                    defaultOption="Selectati remorca"
                    options={allVehicles}
                    error={errors.vehicle}
                    filterText={this.state.filterText}
                    onFilterTextInput={this.handleFilterTextInput}
                />

                <SelectInput
                    name="drivers[1]"
                    label="Al doilea sofer"
                    defaultOption="Selectati sofer"
                    options={allDrivers}
                    error={errors.driver}
                    filterText={this.state.filterText}
                    onFilterTextInput={this.handleFilterTextInput}
                />

                <DateInput
                    selected={this.state.departureDate}
                    onChange={this.departureDateChangeHandler}
                    label="Data plecarii"
                    placeholder="Alegeti data"
                    name="roadmap[departureDate]"
                />
                <DateInput
                    selected={this.state.arrivedDate}
                    onChange={this.arrivedDateChangeHandler}
                    label="Data sosirii"
                    placeholder="Alegeti data"
                    name="roadmap[arrivedDate]"
                />
                <DateInput
                    selected={this.state.valabilityDate}
                    onChange={this.valabilityDateChangeHandler}
                    label="Data valabilitatii foii de parcurs"
                    placeholder="Alegeti data"
                    name="roadmap[valabilityDate]"
                />

                <SelectInput
                    name="roadmap[departureLocality][id]"
                    label="Localitate pornire"
                    defaultOption="Selectati adresa de pornire"
                    options={allAddresses}
                    error={errors.departureLocality}
                    filterText={this.state.filterText}
                    onFilterTextInput={this.handleFilterTextInput}
                />

                <SelectInput
                    name="roadmap[arrivedLocality][id]"
                    label="Localitate destinatie"
                    defaultOption="Selectati adresa de destinatie"
                    options={allAddresses}
                    error={errors.arrivedLocality}
                    filterText={this.state.filterText}
                    onFilterTextInput={this.handleFilterTextInput}
                />

                <TextInput
                    name="roadmap[combustibleQuantity]"
                    label="Cantitate combustibil"
                    error={errors.combustibleQuantity}
                />
                <TextInput
                    name="roadmap[departureDistance]"
                    label="Kilometraj la pornire"
                    error={errors.departureDistance}
                />

                <TextInput
                    name="roadmap[arrivedDistance]"
                    label="Kilometraj la sosire"
                    error={errors.arrivedDistance}
                />

                <input
                    type="submit"
                    disabled={saving}
                    value={'Salveaza'}
                    className="btn btn-primary"
                    onClick={(event) => { event.preventDefault(); var res = serialize(document.querySelector('#roadmapForm'), { hash: true }); console.log(res)}}
                />


            </form>
        );
    }
}

RoadmapForm.propTypes = {
    roadmap: PropTypes.object.isRequired,
    allDrivers: PropTypes.array,
    allVehicles: PropTypes.array,
    allAddresses: PropTypes.array,
    onSave: PropTypes.func.isRequired,
    saving: PropTypes.bool,
    errors: PropTypes.object

};

export default RoadmapForm;