import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import Details from './RoadmapDetails';
import * as roadmapActions from '../../actions/RoadmapsActions';

import RoadmapForm from './RoadmapForm';
import { driversFormattedForDropdown } from '../../selectors/driversSelectors';
import { vehiclesFormattedForDropdown } from '../../selectors/vehiclesSelectors';
import { addressesFormattedForDropdown } from '../../selectors/addressesSelectors';

export class ManageRoadmaps extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            roadmap: Object.assign({}, this.props.roadmap),
            errors: {},
            saving: false
        };

        this.updateRoadmapState = this.updateRoadmapState.bind(this);
        this.saveRoadmap = this.saveRoadmap.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.roadmap.id !== nextProps.roadmap.id) {
            this.setState({roadmap: Object.assign({}, nextProps.roadmap)});
        }
    }

    updateRoadmapState(event) {
        event.persist();
        const field = event.target.name;
        const value = event.target.value;
        // const namespace = event.target.dataset.namespace;
        // let roadmap = this.state.roadmap;
        // console.log(roadmap);
        // roadmap[namespace][field] = event.target.value;
        // return this.setState({roadmap: roadmap});

        this.setState({
            [field]: value
        });
    }
    //
    // courseFormIsValid() {
    //     let formIsValid = true;
    //     const errors = {};
    //
    //     if (this.state.roadmap.title.length < 5) {
    //         errors.title = 'Title must be at least 5 characters';
    //         formIsValid = false;
    //     }
    //
    //     this.setState({errors: errors});
    //     return formIsValid;
    // }

    saveRoadmap(event) {
        event.preventDefault();

        // if (!this.courseFormIsValid()) return;

        this.setState({saving: true});
        this.props.actions.saveRoadmap(this.state.roadmap)
            .then(() => this.redirect())
            .catch((error) => {
                toastr.error(error);
                this.setState({saving: false});
            });
    }

    redirect() {
        this.setState({saving: false});
        // toastr.success('Course saved');
        this.context.router.push('/');
    }

    render() {
        return (
            <div>

                <h1>Adauga foaie de parcurs</h1>
                <RoadmapForm
                    allDrivers={this.props.drivers}
                    allVehicles={this.props.vehicles}
                    allAddresses={this.props.addresses}
                    // onChange={this.updateRoadmapState}
                    onSave={this.saveRoadmap}
                    roadmap={this.state.roadmap}
                    errors={this.state.errors}
                    saving={this.state.saving} />
            </div>

        );
    }
}

ManageRoadmaps.propTypes = {
    roadmap: PropTypes.object.isRequired,
    drivers: PropTypes.array.isRequired,
    vehicles: PropTypes.array.isRequired,
    addresses: PropTypes.array.isRequired,
    actions: PropTypes.object.isRequired
};

ManageRoadmaps.contextTypes = {
    router: PropTypes.object.isRequired
};

function getCourseById(roadmaps, id) {
    const roadmap = roadmaps.filter(roadmap => roadmap.id === id);
    if (roadmap) return roadmap[0];
    return null;
}

function mapStateToProps(state, ownProps) {
    // const roadmapId = ownProps.params.id;

    let roadmap = {
        roadmap: {
            number: "",
            series: "",
            arrivedDate: "",
            departureDate: "",
            combustibleQuantity: "",
            departureDistance: "",
            arrivedDistance: "",
            valabilityDate: "",
            arrivedLocality: {
                id: ""
            },
            departureLocality: {
                id: ""
            }
        },
        drivers: [],
        vehicle: {
            id: ""
        }
    };

    // if (roadmapId && state.roadmaps.length > 0) {
    //     roadmap = getCourseById(state.roadmaps, roadmapId);
    // }

    console.log(state);
    return {
        roadmap: roadmap,
        drivers: driversFormattedForDropdown(state.drivers),
        vehicles: vehiclesFormattedForDropdown(state.vehicles),
        addresses: addressesFormattedForDropdown(state.addresses)
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(roadmapActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageRoadmaps);
