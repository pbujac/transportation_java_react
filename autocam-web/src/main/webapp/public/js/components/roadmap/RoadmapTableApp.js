import React, { Component } from 'react';
import { Table, Thead, Tr, Td, Th }  from 'reactable';
import { Button }  from 'react-bootstrap';

class RoadmapTable extends Component {
  constructor(props) {
    super(props);

    this.handleRowClick = this.handleRowClick.bind(this);
  }


  handleRowClick(roadmap) {
    window.location.href = `/adauga-foaie-de-parcurs/${roadmap.id}`;
    console.log(roadmap);
  }

  render() {
    const {
      roadmaps,
      handleFilterBySelectedDate,
      startDate
    } = this.props;
    return (
      <Table className="table"
             id="table"
             filterable={['firstDriverLN', 'firstDriverFN',
               'arrivedDate', 'arrivedDistance', 'combustibleQuantity', 'departureDate', 'departureDate', 'departureDistance', 'series', 'number' ]}
             sortable={true}
             itemsPerPage={10}
             pageButtonLimit={5}
             defaultSort={{column: 'departureDate', direction: 'desc' }}
             noDataText="Nu au fost gasite foi de parcurs"
      >
        <Thead>
          <Th rowSpan={2} column="series">Seria</Th>
          <Th  column="number">Nr. foii de parcurs</Th>
          <Th column="firstDriverFN">Șofer Prenume</Th>
          <Th  column="firstDriverLN">Șofer Nume</Th>
          <Th  column="departureDate">Data eliberării</Th>
          <Th  column="combustibleQuantity">Combustibil, l</Th>
          <Th column="departureDistance"> Kilometraj la pornire</Th>
          <Th column="arrivedDistance">Kilometraj la sosire</Th>
          <Th column="edit">Editare</Th>
          <Th column="delete">Ștergere</Th>
          <Th column="report">Generare raport</Th>
        </Thead>
        { (startDate ? handleFilterBySelectedDate() : roadmaps).map(roadmap => {
          const {
            id,
            roadmap: {
              number,
              series,
              departureDate,
              combustibleQuantity,
              departureDistance,
              arrivedDistance
            },
            driver: {
              person: {
                name: firstDriverFN,
                surname: firstDriverLN
              },
            }
          } = roadmap;
          return (
            <Tr key={id} onClick={() => {  this.handleRowClick(roadmap) } }>
              <Td column="series">{ series }</Td>
              <Td column="number">{ number }</Td>
              <Td column="firstDriverFN" >{ firstDriverFN }</Td>
              <Td column="firstDriverLN">{ firstDriverLN }</Td>
              <Td column="departureDate" value={ departureDate }>{ departureDate }</Td>

              <Td column="combustibleQuantity">{ combustibleQuantity }</Td>
              <Td column="departureDistance">{ departureDistance }</Td>
              <Td column="arrivedDistance">{ arrivedDistance }</Td>
              <Td column="edit"><Button bsStyle="primary" onClick={(event) => { event.stopPropagation(); console.log('edit') }}>Editare</Button></Td>
              <Td column="delete"><Button bsStyle="danger" onClick={(event) => { event.stopPropagation(); console.log('delete') }}>Ștergere</Button></Td>
              <Td column="report"><Button bsStyle="success" onClick={(event) => { event.stopPropagation(); console.log('report') }}>Generare raport</Button></Td>
            </Tr>
          );
        }) }
      </Table>
    );
  }
}

export default RoadmapTable;

