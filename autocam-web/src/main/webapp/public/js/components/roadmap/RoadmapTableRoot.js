import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import RoadmapTable from './RoadmapTableApp';
import {connect} from 'react-redux';
import * as actions from '../../actions/RoadmapsActions';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';

import 'react-datepicker/dist/react-datepicker.css'
import '../../../css/styles.css';

class RoadmapsRoot extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      startDate: ''
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleFilterBySelectedDate = this.handleFilterBySelectedDate.bind(this);
  }

  componentWillMount() {
    console.log(this.props);
    // if (this.props.roadmaps[0].number === '') {
    //   this.props.actions.loadRoadmapsSuccess();
    // }
  }

  handleChange(date) {
    this.setState({
      startDate: date
    });

  }
  
  handleFilterBySelectedDate() {
    let filterDate = this.state.startDate ? this.state.startDate.locale('en-ca').format('L') : this.state.startDate;
    return this.props.roadmaps.filter((roadmap) => {
      return (roadmap.roadmap.departureDate === filterDate);
    });
  }

  render() {
    return (
      <div>
        <h1>Foi de parcurs</h1>
        <DatePicker
          dateFormat="YYYY-MM-DD"
          selected={this.state.startDate}
          onChange={this.handleChange}
          isClearable={true}
          placeholderText="Filtrare dupa data"
          className="red-border"
        />

        <RoadmapTable
          roadmaps={this.props.roadmaps}
          handleFilterBySelectedDate={this.handleFilterBySelectedDate}
          startDate={this.state.startDate}
        />
      </div>
    );
  }
}

RoadmapsRoot.propTypes = {
  roadmaps: PropTypes.array.isRequired
}

function mapStateToProps(state, ownProps) {
  return {
    roadmaps: state.roadmaps
  };
}

function mapDispatchToProps(dispatch) {
  return {actions: bindActionCreators(actions, dispatch)}
}


export default connect(mapStateToProps, mapDispatchToProps)(RoadmapsRoot);