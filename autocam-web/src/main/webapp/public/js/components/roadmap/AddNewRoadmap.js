import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Route } from 'react-router-dom';
import * as actions from '../../actions/RoadmapsActions';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import Details from './RoadmapDetails';
import RoadmapForm from './RoadmapForm';

class AddNewRoadmap extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      roadmap: Object.assign({}, this.props.roadmap),
      errors: {}
    };

  }


  render() {
    return (
      <div>
        <Route  path='+/adauga-foaie-de-parcurs/:id' component={Details}/>
        <h1>Adauga foaie de parcurs</h1>
        <RoadmapForm
          roadmap={this.state.roadmap}
          allDrivers={[]}
          allVehicles={[]}
          allLocalities={[ {name:'mereni'}, {name:'chisinau'}, {name:'anenii noi'}]}
          loading={true}
          // onSave={}
          // onChange={}
          // loading={}
          errors={this.state.errors}
        />

      </div>
    );
  }
}

AddNewRoadmap.propTypes = {
  roadmap: PropTypes.object.isRequired
}

function mapStateToProps(state, ownProps) {
  let roadmap = {
    series: '',
    number: '',
    firstDriver: '',
    departureDate: '',
    vehicle: '',
    vehicleType: '',
    secondDriver: '',
    combustibleQuantity: '',
    departureDistance: '',
    arrivedDistance: '',
    departureLocality: '',
    arrivedLocality: ''

  };
  return {
    roadmap: roadmap
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddNewRoadmap);