import React from 'react';
import PropTypes from 'prop-types';
import DatePicker from 'react-datepicker';

const DateInput = ({ name, label, onChange, placeholder, selected, error }) => {
    let wrapperClass = 'form-group';
    if(error && error.length > 0) {
        wrapperClass += ' ' + 'has-error';
    }

    return (
        <div className={wrapperClass}>
            <label htmlFor={name}>{label}</label>
            <div className="field">
                <DatePicker
                    dateFormat="YYYY-MM-DD"
                    className="form-control"
                    selected={selected}
                    onChange={onChange}
                    isClearable={true}
                    placeholderText={placeholder}
                    name={name}
                />
                {error && <div className="alert alert-danger">{error}</div>}
            </div>
        </div>
    )
};

DateInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    placeholder: PropTypes.string,
    selected: PropTypes.object,
    error: PropTypes.string
};

export default DateInput;