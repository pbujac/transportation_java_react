import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactModal from 'react-modal';
import Modal from './Modal';
import $  from 'jquery';

ReactModal.setAppElement('#root');

class SelectInput extends Component  {
    constructor(props) {
        super(props);

        this.state = {
            // showModal: this.props.isOpen,
            // errors: {},
            filter: ''
        };

        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleFilterText = this.handleFilterText.bind(this);
        this.handleFilterResults = this.handleFilterResults.bind(this);
    }

    handleOpenModal () {
        this.setState({ showModal: true });
    }

    handleCloseModal () {
        this.setState({ showModal: false });
    }
    handleFilterText (e) {
        this.setState({ filter: e.target.value });
    }

    handleFilterResults () {
        return this.props.options.filter((option) => {
            // let filter = $('#filter').value;
            console.log(this.state.filter);
            console.log(option.text);
            console.log(option.text.includes(this.state.filter));
            return option.text.toLowerCase().includes(this.state.filter.toLowerCase());
        })
    }
    render() {
        const {name, label, defaultOption, error, options} = this.props;
        console.log(this.handleFilterResults());

        return (
            <div className="form-group">
                <label htmlFor={name}>{label}</label>
                <div className="field">
                    {/*<select*/}
                        {/*name={name}*/}
                        {/*className="form-control">*/}
                        {/*<option value="">{defaultOption}</option>*/}
                        {/*{options.map((option) => {*/}
                            {/*return <option key={option.value} value={option.value}>{option.text}</option>;*/}
                        {/*})*/}
                        {/*}*/}
                    {/*</select>*/}
                    <input type="text" name={name} role="combo" list={"options" + name} className="filter form-control" value={this.state.filter} onChange={this.handleFilterText}/>
                    <datalist
                        id={"options" + name}

                        // className="form-control"
                    >

                        <option value="">{defaultOption}</option>
                        {options.length > 0 ? options.map((option) => {
                            return <option key={option.value} value={option.text}>{option.value}</option>;
                        }) : <option value="">Nu au fost gasite rezultate pentru {this.state.filter}</option>
                        }
                    </datalist>
                    <div className="modal-trigger">
                        { ( this.handleFilterResults().length === 0) && <Modal contentLabel="addNew"/>
                        }
                    </div>
                    {/*{options.length === 0 &&*/}
                    {/*<button className="btn btn-success" onClick={this.showModal} type="button">Adauga</button>*/}
                    {/*}*/}
                    {/*<Modal isOpen="" contentLabel=""/>*/}
                    {error && (<div className="alert alert-danger">{error}</div>)}
                </div>
            </div>
        );
    }
};

SelectInput.propTypes = {
    name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    filter: PropTypes.string,
    defaultOption: PropTypes.string,
    error: PropTypes.string,
    options: PropTypes.arrayOf(PropTypes.object)
};

export default SelectInput;
