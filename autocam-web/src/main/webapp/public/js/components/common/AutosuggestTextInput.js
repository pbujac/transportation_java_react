import React, { Component } from 'react';
import Autosuggest from 'react-autosuggest';
import ReactModal from 'react-modal';

// import AddNewDriver from '../driver/components/AddNewDriver';
import Modal from './Modal'

const getSuggestionValue = suggestion => suggestion.name;

const renderSuggestion = suggestion => (
  <div>
    {suggestion.name}
  </div>
);
class AutosuggestTextInput extends Component  {
  constructor(props) {
    super(props);

    this.state = {
      errors: {},
      value: this.props.value,
      suggestions: this.props.suggestions
    };

    this.onChange = this.onChange.bind(this);
    this.onSuggestionsFetchRequested = this.onSuggestionsFetchRequested.bind(this);
    this.onSuggestionsClearRequested = this.onSuggestionsClearRequested.bind(this);
    this.getSuggestions = this.getSuggestions.bind(this);

  }

  getSuggestions(value) {
  const inputValue = value.trim().toLowerCase();
  const inputLength = inputValue.length;
  console.log(value);

  return inputLength === 0 ? [] : this.props.suggestions.filter(suggestion =>
    suggestion.name.toLowerCase().slice(0, inputLength) === inputValue
  );
};

  onChange (event, { newValue }) {
    this.setState({
      value: newValue
    });
  };

  onSuggestionsFetchRequested ({ value }) {
    this.setState({
      suggestions: this.getSuggestions(value)
    });
  };

  onSuggestionsClearRequested() {
    this.setState({
      suggestions: []
    });
  };
  render() {
    let wrapperClass = 'form-group';
    if(error && error.length > 0) {
      wrapperClass += ' ' + 'has-error';
    }

    const {
      roadmap,
      name,
      label,
      placeholder,
      error } = this.props;
    const { value, suggestions } = this.state;
    const inputProps = {
      placeholder: placeholder,
      value,
      type: 'text',
      name: name,
      className: 'form-control',
      onChange: this.onChange
    };

    console.log(this.props);
    console.log(suggestions);
    return (
      <div className={wrapperClass}>
        <label htmlFor={name}>{label}</label>
        <div className="field">

            <Autosuggest
              suggestions={suggestions}
              onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
              onSuggestionsClearRequested={this.onSuggestionsClearRequested}
              getSuggestionValue={getSuggestionValue}
              renderSuggestion={renderSuggestion}
              inputProps={inputProps}
            />


          {error && <div className="alert alert-danger">{error}</div>}
        </div>
        { suggestions.length !== 0 ? ''
          :

          <Modal contentLabel="Forma de adaugare element nou"/>
        }
      </div>

    );
  }
}

export default AutosuggestTextInput;


