import React, { Component } from 'react';
import ReactModal from 'react-modal';
import AddNewDriver from '../driver/AddNewDriver';

ReactModal.setAppElement('#root');

class Modal extends Component  {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      errors: {},
    };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
  }

  handleOpenModal () {
    this.setState({ showModal: true });
  }

  handleCloseModal () {
    this.setState({ showModal: false });
  }


  render() {
    return (
      <div>
        <button type="button" className="btn btn-primary inline" onClick={this.handleOpenModal}>Adaugă element nou</button>
        <ReactModal
          isOpen={this.state.showModal}
          contentLabel={this.props.contentLabel}
        >
          <button  className="btn btn-danger" onClick={this.handleCloseModal}>Închide</button>
          <h1>Adauga șofer</h1>
          <AddNewDriver/>
        </ReactModal>
      </div>
    );
  }
}

export default Modal;
//
// import React, { Component } from 'react';
// import ReactModal from 'react-modal';
// import AddNewDriver from '../driver/AddNewDriver';
//
// ReactModal.setAppElement('#root');
//
// const Modal = ({handleOpenModal, showModal, contentLabel, handleCloseModal})  => {
//     return (
//         <div>
//           <button type="button" className="btn btn-primary inline" onClick={this.handleOpenModal}>Adaugă element nou</button>
//           <ReactModal
//               isOpen={this.state.showModal}
//               contentLabel={this.props.contentLabel}
//           >
//             <button  className="btn btn-danger" onClick={this.handleCloseModal}>Închide</button>
//             <h1>Adauga șofer</h1>
//               {/*<AddNewDriver/>*/}
//           </ReactModal>
//         </div>
//     );
// };
//
// export default Modal;
//




