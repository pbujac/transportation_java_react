import React, { Component } from 'react';
import PropTypes from 'prop-types';

import DatePicker from 'react-datepicker';
import TextInput from '../common/TextInput';
import AutosuggestTextInput from '../common/AutosuggestTextInput';

class DriverForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: ''
    };
    this.handleChange = this.handleChange.bind(this);
  }


  handleChange(date) {
    this.setState({
      startDate: date
    });

  }
  render() {
    const { driver, allJobs, allAddresses, onSave, onChange, loading, errors } = this.props;
    return (
      <form>
        <TextInput
          name="person.name"
          label="Nume"
          value={driver.person.name}
          onChange={onChange}
          error={errors.name}
        />
        <TextInput
          name="person.surname"
          label="Prenume"
          value={driver.person.surname}
          onChange={onChange}
          error={errors.surname}
        />


        <label htmlFor="birthdayDate">Data eliberarii foii de parcurs</label>
        <DatePicker
          dateFormat="YYYY-MM-DD"
          selected={this.state.startDate}
          onChange={this.handleChange}
          isClearable={true}
          placeholderText="Ziua de naștere"
          name="person.birthdayDate"
          value={driver.person.birthdayDate}
        />

        {/*<AutosuggestTextInput*/}
          {/*name="person.address"*/}
          {/*label="Adresa"*/}
          {/*value={driver.person.address}*/}
          {/*suggestions={[{name: 'moldova'}, {name: 'Ukraina'}]}*/}
          {/*error={errors.address}*/}
        {/*/>*/}

        {/*<AutosuggestTextInput*/}
          {/*name="employeeJob.employeeType"*/}
          {/*label="Funcția deținută"*/}
          {/*value={driver.employeeJob.employeeType}*/}
          {/*suggestions={allJobs}*/}
          {/*// error={errors.employeeJob.employeeType}*/}
        {/*/>*/}


        <input
          type="submit"
          disabled={loading}
          value={loading ? 'Se salveaza...' : 'Salveaza'}
          className="btn btn-primary"
          onClick={onSave}
        />


      </form>
    );
  }
};

DriverForm.propTypes = {
  driver: PropTypes.object.isRequired,
  allJobs: PropTypes.array,
  allAddresses: PropTypes.array,
  onSave: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  errors: PropTypes.object

};

export default DriverForm;