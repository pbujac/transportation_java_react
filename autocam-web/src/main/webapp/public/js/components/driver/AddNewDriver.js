import React, { Component } from 'react';
import {connect} from 'react-redux';
import * as actions from '../../actions/DriverActions';
import PropTypes from 'prop-types';
import {bindActionCreators} from 'redux';
import DriverForm from './DriverForm';

class AddNewDriver extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      driver: {
        "company":{
          "name":"testName",
          "address":{
            "name":"testAddress"
          }
        },
        "person":{
          "name":"testName",
          "address":{
            "name":"testAddress"
          },
          "surname":"test"
        },
        "employeeJob":{"employeeType":"SOFER"}},
      errors: {}
    };

  }


  render() {
    return (
      <div>
        <DriverForm
          driver={this.state.driver}
          allDrivers={[]}
          allJobs={[{name: 'SOFER'}, {name: 'MANAGER'}, {name: 'CONTABIL'}]}
          onSave={() => console.log('save')}
          onChange={() => console.log('save')}
          // loading={}
          errors={this.state.errors}
        />

      </div>
    );
  }
}

AddNewDriver.propTypes = {
  driver: PropTypes.object
};

function mapStateToProps(state, ownProps) {
  let driver = {
    company: {
      name: '',
      address: {
        name: ''
      }
    },
    person: {
      name: '',
      address: {
        name: ''
      },
      surname: ''
    },
    employeeJob: {
      employeeType: 'SOFER'
    }
  };
  return {
    driver: driver
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(AddNewDriver);