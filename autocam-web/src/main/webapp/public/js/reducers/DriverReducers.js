import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function driversReducer(state = initialState.drivers, action) {
    switch (action.type) {
        case types.LOAD_DRIVERS_SUCCESS:
            console.log(action);
            return action.drivers;
        case types.CREATE_DRIVER_SUCCESS:
            return [ ...state,
                Object.assign({}, action.driver)
            ];
            return state;
        case types.UPDATE_DRIVER_SUCCESS:
            return [
                ...state.filter(driver => driver.id !== action.driver.id),
                Object.assign({}, action.driver)
            ];
        default:
            return state;
    }

}
