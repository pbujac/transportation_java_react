import { combineReducers } from 'redux';
import roadmaps from './RoadmapsReducers';
import drivers from './DriverReducers';
import vehicles from './VehicleReducers';
import addresses from './AddressReducers';
import ajaxCallsInProgress from './ajaxStatusReducer';

export const rootReducer = combineReducers({
    roadmaps,
    drivers,
    vehicles,
    addresses,
    ajaxCallsInProgress
});

export default rootReducer;