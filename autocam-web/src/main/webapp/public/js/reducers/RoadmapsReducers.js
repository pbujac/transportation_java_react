import * as types from '../actions/actionTypes';
import initialState from './initialState';
export default function roadmapsReducer(state = initialState.roadmaps, action) {
    switch (action.type) {
        case types.LOAD_ROADMAPS_SUCCESS:
            console.log(action);
            return action.roadmaps;
        case types.CREATE_ROADMAP_SUCCESS:
            return [ ...state,
                Object.assign({}, action.roadmap)
            ];
            return state;
        case types.UPDATE_ROADMAP_SUCCESS:
            return [
                ...state.filter(roadmap => roadmap.id !== action.roadmap.id),
                Object.assign({}, action.roadmap)
            ];
        default:
            return state;
    }

}
