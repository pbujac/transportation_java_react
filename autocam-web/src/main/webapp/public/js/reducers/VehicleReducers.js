import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function vehiclesReducer(state = initialState.vehicles, action) {
    switch (action.type) {
        case types.LOAD_VEHICLES_SUCCESS:
            console.log(action);
            return action.vehicles;
        case types.CREATE_VEHICLE_SUCCESS:
            return [ ...state,
                Object.assign({}, action.vehicle)
            ];
            return state;
        case types.UPDATE_VEHICLE_SUCCESS:
            return [
                ...state.filter(vehicle => vehicle.id !== action.vehicle.id),
                Object.assign({}, action.vehicle)
            ];
        default:
            return state;
    }

}
