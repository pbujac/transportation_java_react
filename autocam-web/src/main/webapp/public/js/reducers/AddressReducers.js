import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function addressesReducer(state = initialState.addresses, action) {
    switch (action.type) {
        case types.LOAD_ADDRESSES_SUCCESS:
            console.log(action);
            return action.addresses;
        case types.CREATE_ADDRESS_SUCCESS:
            return [ ...state,
                Object.assign({}, action.address)
            ];
            return state;
        case types.UPDATE_ADDRESS_SUCCESS:
            return [
                ...state.filter(address => address.id !== action.address.id),
                Object.assign({}, action.address)
            ];
        default:
            return state;
    }

}
