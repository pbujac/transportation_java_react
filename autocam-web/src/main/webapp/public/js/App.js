import React, { Component } from 'react';
import { Route, Redirect, Link, Switch, withRouter  } from 'react-router-dom';
import RoadmapsRoot from './components/roadmap/RoadmapTableRoot';
// import AddNewRoadmap from './components/roadmap/AddNewRoadmap'
import ManageRoadmaps from './components/roadmap/ManageRoadmaps'
// import Roadmaps from './api/data';
import Details from './components/roadmap/RoadmapDetails';


const NoMatch = () => <div><h1>Page not found</h1></div>;

class App extends Component  {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div>
          <h2>Autocam Service</h2>
          <p><Link to="/" >Foi de parcurs</Link></p>
          <p><Link to="/adauga-foaie-de-parcurs" >Adauga foaie de parcurs</Link></p>
        </div>
       <Switch>
         <Route exact path='/' render={() => <RoadmapsRoot />}/>
         <Route  path='/adauga-foaie-de-parcurs' component={ManageRoadmaps}/>
           <Route  path='/adauga-foaie-de-parcurs/:id' component={Details}/>
         <Route  path='*' component={NoMatch}/>
         <Redirect from='*' push to='/' />
       </Switch>
      </div>
    );
  }
}


export default withRouter(App);

