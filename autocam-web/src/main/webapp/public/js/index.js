import 'jquery';
import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import { BrowserRouter } from 'react-router-dom';
import configureStore from './store/RoadmapStore';
import { Provider } from 'react-redux';
import { loadRoadmaps } from './actions/RoadmapsActions';
import { loadDrivers } from './actions/DriverActions';
import { loadVehicles } from './actions/VehiclesActions';
import { loadAddresses } from './actions/AddressActions';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import '../css/styles.css';

const store = configureStore();
store.dispatch(loadRoadmaps());
store.dispatch(loadDrivers());
store.dispatch(loadVehicles());
store.dispatch(loadAddresses());

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

