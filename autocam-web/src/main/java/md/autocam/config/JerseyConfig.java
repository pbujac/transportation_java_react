package md.autocam.config;


import md.autocam.address.service.AddresServiceImpl;
import md.autocam.employee.service.EmployeeJobServiceImpl;
import md.autocam.employee.service.EmployeeServiceImpl;
import md.autocam.person.service.LegalPersonServiceImpl;
import md.autocam.person.service.LegalPersonTypeServiceImpl;
import md.autocam.person.service.NaturalPersonServiceImpl;
import md.autocam.roadmap.service.MedicalEvidenceServiceImpl;
import md.autocam.roadmap.service.RoadmapServiceImpl;
import md.autocam.roadmap.service.RoadmapVehicleServiceImpl;
import md.autocam.vehicle.service.VehicleBrandServiceImpl;
import md.autocam.vehicle.service.VehicleModelServiceImpl;
import md.autocam.vehicle.service.VehicleServiceImpl;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.ws.rs.ApplicationPath;


@Component
@ApplicationPath("/api")
public class JerseyConfig extends ResourceConfig {

    /**
     * In constructor we can define Jersey Resources & Other Components
     */
    public JerseyConfig() {
        register(RoadmapVehicleServiceImpl.class);
        register(MedicalEvidenceServiceImpl.class);
        register(AddresServiceImpl.class);
        register(LegalPersonServiceImpl.class);
        register(LegalPersonTypeServiceImpl.class);
        register(NaturalPersonServiceImpl.class);
        register(EmployeeJobServiceImpl.class);
        register(EmployeeServiceImpl.class);
        register(VehicleBrandServiceImpl.class);
        register(VehicleModelServiceImpl.class);
        register(VehicleServiceImpl.class);
        register(RoadmapServiceImpl.class);
    }
}