package md.autocam;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class AutocamWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutocamWebApplication.class, args);
	}

}
