package md.autocam.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by pit on 4/26/17.
 */
@Controller
public class HomeController {

    @RequestMapping(value =  {"/", "/adauga-foaie-de-parcurs/**"})
    public String index() {
        return "index";
    }

    @GetMapping("/{path:(?!.*.js|.*.css|.*.jpg).*$}")
    public String allMapping() {
        return "index";
    }

}
