package md.autocam.users.repository;

import md.autocam.model.user.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pit on 4/28/17.
 */

//@RepositoryRestResource(collectionResourceRel = "item", path = "item")
@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {
}
