package md.autocam.users.repository;

import md.autocam.model.user.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pit on 4/28/17.
 */

//@RepositoryRestResource(collectionResourceRel = "item", path = "item")
@Repository
public interface UserRepository extends CrudRepository<User, Long> {
}
