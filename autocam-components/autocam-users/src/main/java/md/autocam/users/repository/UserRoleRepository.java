package md.autocam.users.repository;

import md.autocam.model.user.UserRole;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pit on 5/1/17.
 */
//@RepositoryRestResource(path = "user-role")
public interface UserRoleRepository extends CrudRepository<UserRole, Long> {
}
