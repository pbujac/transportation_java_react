package md.autocam.users;

import md.autocam.model.user.Role;
import md.autocam.users.repository.RoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Created by pit on 4/29/17.
 */
@SpringBootApplication
public class AutocamUsersApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutocamUsersApplication.class, args);
    }

    @Bean
    public CommandLineRunner roleDemo(RoleRepository roleRepository) {
        return (args) -> {
            roleRepository.save(new Role("ADMIN"));
            roleRepository.save(new Role("MANAGER"));
        };
    }
}
