package md.autocam.service;

import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

/**
 * Created by pit on 5/14/17.
 */
public interface ICrudService<T, K extends Serializable> {

    List<T> getAll();

    Page<T> getAllPaginate(int size, int page);

    T save(T t);

    void delete(K k);

    T update(T t, K k);

    T findById(K k);

}
