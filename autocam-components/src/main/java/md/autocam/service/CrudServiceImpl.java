package md.autocam.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.web.bind.annotation.RequestBody;

import javax.ws.rs.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by pit on 5/14/17.
 */

@Produces("application/json")
public class CrudServiceImpl<T, K extends Serializable> implements ICrudService<T, K> {

    private PagingAndSortingRepository<T, K> repositoryCrud;

    public CrudServiceImpl(PagingAndSortingRepository<T, K> repositoryCrud) {
        this.repositoryCrud = repositoryCrud;
    }

    @Override
    @GET
    public List<T> getAll() {
        return (List<T>) repositoryCrud.findAll();
    }

    @Override
    @GET
    @Path("/paginate")
    public Page<T> getAllPaginate(@QueryParam("page") @DefaultValue("0") int page,
                                  @QueryParam("size") @DefaultValue("20") int size) {
        return repositoryCrud.findAll(new PageRequest(page, size));
    }

    @Override
    @POST
    public T save(@RequestBody T t) {
        return repositoryCrud.save(t);
    }

    @Override
    @DELETE
    @Path("/{id}")
    public void delete(@PathParam("id") K k) {
        repositoryCrud.delete(k);
    }

    @Override
    @PUT
    @Path("/{id}")
    public T update(@RequestBody T t, @PathParam("id") K id) {
        return repositoryCrud.findOne(id);
    }

    @Override
    @GET
    @Path("/{id}")
    public T findById(@PathParam("id") K k) {
        return repositoryCrud.findOne(k);
    }
}
