package md.autocam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutocamComponentsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutocamComponentsApplication.class, args);
	}
}
