package md.autocam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutocamVehicleApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutocamVehicleApplication.class, args);
	}
}
