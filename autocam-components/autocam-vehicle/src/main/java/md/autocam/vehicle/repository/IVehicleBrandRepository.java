package md.autocam.vehicle.repository;

import md.autocam.model.vehicle.Vehicle;
import md.autocam.model.vehicle.VehicleBrand;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by pit on 5/1/17.
 */
//@RepositoryRestResource(path = "vehicle-brand")
public interface IVehicleBrandRepository extends PagingAndSortingRepository<VehicleBrand, Long> {
}
