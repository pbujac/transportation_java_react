package md.autocam.vehicle.service;

import md.autocam.model.vehicle.VehicleBrand;
import md.autocam.service.CrudServiceImpl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by pit on 5/14/17.
 */

@Service
@Path("/vehicle-brand")
public class VehicleBrandServiceImpl extends CrudServiceImpl<VehicleBrand, Long> {

    public VehicleBrandServiceImpl(PagingAndSortingRepository<VehicleBrand, Long> repositoryCrud) {
        super(repositoryCrud);
    }
}


