package md.autocam.vehicle.service;

import md.autocam.model.vehicle.Vehicle;
import md.autocam.model.vehicle.VehicleModel;
import md.autocam.service.CrudServiceImpl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.Path;

/**
 * Created by pit on 5/14/17.
 */

@Service
@Path("/vehicle")
public class VehicleServiceImpl extends CrudServiceImpl<Vehicle, Long> {

    public VehicleServiceImpl(PagingAndSortingRepository<Vehicle, Long> repositoryCrud) {
        super(repositoryCrud);
    }
}


