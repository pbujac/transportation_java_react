package md.autocam.vehicle.service;

import md.autocam.model.vehicle.VehicleBrand;
import md.autocam.model.vehicle.VehicleModel;
import md.autocam.service.CrudServiceImpl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.Path;

/**
 * Created by pit on 5/14/17.
 */

@Service
@Path("/vehicle-model")
public class VehicleModelServiceImpl extends CrudServiceImpl<VehicleModel, Long> {

    public VehicleModelServiceImpl(PagingAndSortingRepository<VehicleModel, Long> repositoryCrud) {
        super(repositoryCrud);
    }
}


