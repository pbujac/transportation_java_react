package md.autocam.vehicle.repository;

import md.autocam.model.vehicle.VehicleBrand;
import md.autocam.model.vehicle.VehicleModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by pit on 5/1/17.
 */
//@RepositoryRestResource(path = "vehicle-model")
public interface IVehicleModelRepository extends PagingAndSortingRepository<VehicleModel, Long> {
}
