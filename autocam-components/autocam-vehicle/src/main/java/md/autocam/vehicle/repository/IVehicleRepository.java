package md.autocam.vehicle.repository;

import md.autocam.model.vehicle.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by pit on 5/1/17.
 */
//@RepositoryRestResource(path = "vehicles")
public interface IVehicleRepository extends PagingAndSortingRepository<Vehicle, Long> {
}
