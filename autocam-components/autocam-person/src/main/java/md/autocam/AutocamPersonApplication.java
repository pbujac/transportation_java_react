package md.autocam;

import md.autocam.employee.repository.EmployeeJobRepository;
import md.autocam.employee.repository.EmployeeRepository;
import md.autocam.model.employee.Employee;
import md.autocam.model.employee.EmployeeJob;
import md.autocam.model.person.LegalPerson;
import md.autocam.model.person.LegalPersonType;
import md.autocam.model.person.NaturalPerson;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.Date;

@SpringBootApplication
public class AutocamPersonApplication {

	public static void main(String[] args) {
	    SpringApplication.run(AutocamPersonApplication.class, args);
	}

    @Bean
    public CommandLineRunner employeeDemo(EmployeeRepository employeeRepository) {
        return (args) -> {

//            java.util.Date utilDate = new java.util.Date();
//			employeeRepository.save(new Employee(
//                    new LegalPerson("1004600066126", new LegalPersonType("SRL")),
//                    new NaturalPerson("test" , new Date(utilDate.getTime())),
//                    new EmployeeJob("SOFER")
//            ));
//			employeeRepository.save(new Employee(
//                    new LegalPerson("10304600066126", new LegalPersonType("SRL")),
//                    new NaturalPerson("test3" , new Date(utilDate.getTime())),
//                    new EmployeeJob("SOFER")
//            ));
        };
    }
}
