package md.autocam.employee.repository;

import md.autocam.model.employee.Employee;
import md.autocam.model.employee.EmployeeJob;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by pit on 5/1/17.
 */
//@RepositoryRestResource( path = "employee-job")
public interface EmployeeJobRepository extends PagingAndSortingRepository<EmployeeJob, Long> {
}
