package md.autocam.employee.service;

import md.autocam.model.employee.Employee;

import java.util.List;

/**
 * Created by pit on 5/14/17.
 */
public interface IEmployeeService {

    List<Employee> findByEmployeeJob(String job);
}
