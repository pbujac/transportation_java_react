package md.autocam.employee.service;

import md.autocam.employee.repository.EmployeeRepository;
import md.autocam.model.employee.Employee;
import md.autocam.model.employee.EmployeeJob;
import md.autocam.service.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.Path;
import java.util.List;

/**
 * Created by pit on 5/14/17.
 */

@Service
@Path("/employee-job")
public class EmployeeJobServiceImpl extends CrudServiceImpl<EmployeeJob, Long>  {


    public EmployeeJobServiceImpl(PagingAndSortingRepository<EmployeeJob, Long> repositoryCrud) {
        super(repositoryCrud);
    }


}
