package md.autocam.employee.service;

import md.autocam.employee.repository.EmployeeRepository;
import md.autocam.model.employee.Employee;
import md.autocam.model.person.LegalPerson;
import md.autocam.service.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.List;

/**
 * Created by pit on 5/14/17.
 */

@Service
@Path("/employee")
public class EmployeeServiceImpl extends CrudServiceImpl<Employee, Long> implements IEmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(PagingAndSortingRepository<Employee, Long> repositoryCrud) {
        super(repositoryCrud);
    }

    @Override
    @GET
    @Path("/search/job/{job}")
    public List<Employee> findByEmployeeJob(@PathParam("job") String job) {
        return employeeRepository.findByEmployeeJobEmployeeType(job);
    }
}
