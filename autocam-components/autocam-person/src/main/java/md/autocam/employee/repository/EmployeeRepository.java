package md.autocam.employee.repository;

import md.autocam.model.employee.Employee;
import md.autocam.model.employee.EmployeeJob;
import md.autocam.model.person.LegalPerson;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by pit on 5/1/17.
 */
//@RepositoryRestResource(path = "employee")
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Long> {

    List<Employee> findByEmployeeJobEmployeeType(@Param("employeeType") String employeeType);
}
