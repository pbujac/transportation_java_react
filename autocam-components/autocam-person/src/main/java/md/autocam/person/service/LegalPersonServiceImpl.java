package md.autocam.person.service;

import md.autocam.model.person.LegalPerson;
import md.autocam.service.CrudServiceImpl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by pit on 5/14/17.
 */

@Service
@Path("/legal-person")
public class LegalPersonServiceImpl extends CrudServiceImpl<LegalPerson,Long> {

    public LegalPersonServiceImpl(PagingAndSortingRepository<LegalPerson, Long> repositoryCrud) {
        super(repositoryCrud);
    }
}
