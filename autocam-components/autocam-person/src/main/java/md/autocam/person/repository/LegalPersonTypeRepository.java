package md.autocam.person.repository;

import md.autocam.model.person.LegalPersonType;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pit on 5/1/17.
 */
//@RepositoryRestResource(path = "legal-person-type")
public interface LegalPersonTypeRepository extends PagingAndSortingRepository<LegalPersonType, Long> {
}
