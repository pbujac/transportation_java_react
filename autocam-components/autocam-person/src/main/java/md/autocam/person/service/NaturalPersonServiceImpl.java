package md.autocam.person.service;

import md.autocam.model.person.LegalPerson;
import md.autocam.model.person.NaturalPerson;
import md.autocam.service.CrudServiceImpl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 * Created by pit on 5/14/17.
 */

@Service
@Path("/natural-person")
public class NaturalPersonServiceImpl extends CrudServiceImpl<NaturalPerson,Long> {

    public NaturalPersonServiceImpl(PagingAndSortingRepository<NaturalPerson, Long> repositoryCrud) {
        super(repositoryCrud);
    }
}
