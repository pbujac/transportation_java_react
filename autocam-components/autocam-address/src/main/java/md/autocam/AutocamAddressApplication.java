package md.autocam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutocamAddressApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutocamAddressApplication.class, args);
	}
}
