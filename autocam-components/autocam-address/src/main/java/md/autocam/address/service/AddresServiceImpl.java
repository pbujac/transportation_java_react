package md.autocam.address.service;

import md.autocam.model.address.BaseAddress;
import md.autocam.service.CrudServiceImpl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.Path;

/**
 * Created by pit on 5/14/17.
 */

@Service
@Path("/address")
public class AddresServiceImpl extends CrudServiceImpl<BaseAddress,Long> {

    public AddresServiceImpl(PagingAndSortingRepository<BaseAddress, Long> repositoryCrud) {
        super(repositoryCrud);
    }
}
