package md.autocam.address.repository;

import md.autocam.model.address.BaseAddress;
import md.autocam.model.address.Country;
import md.autocam.model.address.Locality;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * Created by pit on 5/1/17.
 */
//@RepositoryRestResource(path = "address")
public interface AddressRepository extends PagingAndSortingRepository<BaseAddress, Long> {
}
