package md.autocam.roadmap.repository;

import md.autocam.model.medical_evidence.MedicalEvidence;
import md.autocam.model.roadmap.RoadmapVehicle;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

//import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by pit on 5/1/17.
 */

@Repository
public interface IMedicalEvidenceRepository extends PagingAndSortingRepository<MedicalEvidence, Long> {

    List<RoadmapVehicle> findByRoadmapNumber(@Param("number") String number);
}
