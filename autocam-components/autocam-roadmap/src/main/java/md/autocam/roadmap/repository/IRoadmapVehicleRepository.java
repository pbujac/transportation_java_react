package md.autocam.roadmap.repository;

import md.autocam.model.roadmap.RoadmapVehicle;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
//import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

/**
 * Created by pit on 5/1/17.
 */

@Repository
public interface IRoadmapVehicleRepository extends PagingAndSortingRepository<RoadmapVehicle, Long> {

    List<RoadmapVehicle> findByRoadmapNumber(@Param("number") String number);
}
