package md.autocam.roadmap.service;

import md.autocam.model.roadmap.RoadmapVehicle;
import md.autocam.roadmap.repository.IRoadmapVehicleRepository;
import md.autocam.service.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;
import java.util.List;

/**
 * Created by pit on 5/1/17.
 */

@Service
@Path("/roadmap-vehicle")
public class RoadmapVehicleServiceImpl extends CrudServiceImpl<RoadmapVehicle, Long> {

    public RoadmapVehicleServiceImpl(PagingAndSortingRepository<RoadmapVehicle, Long> repositoryCrud) {
        super(repositoryCrud);
    }

}
