package md.autocam.roadmap.service;

import md.autocam.model.roadmap.Roadmap;
import md.autocam.model.roadmap.RoadmapVehicle;
import md.autocam.service.CrudServiceImpl;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.Path;

/**
 * Created by pit on 5/1/17.
 */

@Service
@Path("/roadmap")
public class RoadmapServiceImpl extends CrudServiceImpl<Roadmap, Long> {

    public RoadmapServiceImpl(PagingAndSortingRepository<Roadmap, Long> repositoryCrud) {
        super(repositoryCrud);
    }

}
