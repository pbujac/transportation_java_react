package md.autocam.roadmap.repository;

import md.autocam.model.roadmap.Roadmap;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by pit on 5/1/17.
 */
@Repository
public interface IRoadmapRepository extends PagingAndSortingRepository<Roadmap, Long> {
}
