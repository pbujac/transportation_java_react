package md.autocam.roadmap.service;

import md.autocam.model.medical_evidence.MedicalEvidence;
import md.autocam.roadmap.repository.IMedicalEvidenceRepository;
import md.autocam.service.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.ws.rs.*;

/**
 * Created by pit on 5/1/17.
 */

@Service
@Path("/medical-evidence")
public class MedicalEvidenceServiceImpl extends CrudServiceImpl<MedicalEvidence, Long> {

    public MedicalEvidenceServiceImpl(PagingAndSortingRepository<MedicalEvidence, Long> repositoryCrud) {
        super(repositoryCrud);
    }
}
