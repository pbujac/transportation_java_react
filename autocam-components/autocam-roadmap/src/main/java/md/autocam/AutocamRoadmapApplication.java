package md.autocam;

import md.autocam.model.address.BaseAddress;
import md.autocam.model.employee.Employee;
import md.autocam.model.employee.EmployeeJob;
import md.autocam.model.person.LegalPerson;
import md.autocam.model.person.LegalPersonType;
import md.autocam.model.person.NaturalPerson;
import md.autocam.model.roadmap.Roadmap;
import md.autocam.model.roadmap.RoadmapVehicle;
import md.autocam.model.vehicle.Vehicle;
import md.autocam.model.vehicle.VehicleBrand;
import md.autocam.model.vehicle.VehicleModel;
import md.autocam.roadmap.repository.IRoadmapRepository;
import md.autocam.roadmap.repository.IRoadmapVehicleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.sql.Date;

@SpringBootApplication
public class AutocamRoadmapApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutocamRoadmapApplication.class, args);
    }

    @Bean
    public CommandLineRunner roadmapDemo(IRoadmapRepository IRoadmapRepository) {
        return (args) -> {
//            java.util.Date utilDate = new java.util.Date();
//            IRoadmapRepository.save(new Roadmap("series XY", "nr3232", new Date(utilDate.getTime()),
//                    new Date(utilDate.getTime()),
//                    new BaseAddress("departure"), new BaseAddress("arrived"),
//                    22F, 22F, 33F));
        };
    }

    @Bean
    public CommandLineRunner roadmapVehicleDemo(IRoadmapVehicleRepository IRoadmapVehicleRepository) {
        return (args) -> {
//            java.util.Date utilDate = new java.util.Date();
//            IRoadmapVehicleRepository.save(new RoadmapVehicle(new Employee(
//                    new LegalPerson("1004600066126", new LegalPersonType("SRL")),
//                    new NaturalPerson("test", new Date(utilDate.getTime())),
//                    new EmployeeJob("SOFER")
//            ),
//                    new Vehicle("AN BD 577", new VehicleModel("Corolla", new VehicleBrand("TOYOTA"))),
//                    new Roadmap("series XY", "nr3232", new Date(utilDate.getTime()),
//                            new Date(utilDate.getTime()),
//                            new BaseAddress("departure"), new BaseAddress("arrived"),
//                            22F, 22F, 33F)));
//
//            IRoadmapVehicleRepository.save(new RoadmapVehicle(new Employee(
//                    new LegalPerson("1004600066126", new LegalPersonType("SRL")),
//                    new NaturalPerson("test", new Date(utilDate.getTime())),
//                    new EmployeeJob("SOFER")
//            ),
//                    new Vehicle("AN BD 578", new VehicleModel("Corolla", new VehicleBrand("TOYOTA"))),
//                    new Roadmap("series XY", "nr3232", new Date(utilDate.getTime()),
//                            new Date(utilDate.getTime()),
//                            new BaseAddress("departure"), new BaseAddress("arrived"),
//                            22F, 22F, 33F)));
        };
    }
}
